import tkinter as tk
from tkinter import messagebox
from db import db
from mainPage import MainPage
import os,sys

class LoginPage:
    def __init__(self, master):
        self.root = master
        self.root.geometry('300x180+800+400')
        self.root.iconbitmap(self.resource_path('vth.ico'))

        self.username = tk.StringVar()
        self.passwd = tk.StringVar()

        self.page = tk.Frame(root)
        self.page.pack()

        tk.Label(self.page).grid(row=0, column=0)

        tk.Label(self.page, text='账户：').grid(row=1, column=1)
        tk.Entry(self.page, textvariable=self.username).grid(row=1, column=2,pady=10)

        tk.Label(self.page, text='密码：').grid(row=2, column=1)
        enterred = tk.Entry(self.page, textvariable=self.passwd, show='*')
        enterred.grid(row=2, column=2,pady=10)
        enterred.bind("<Return>", self.login)

        enterred = tk.Button(self.page, text='登录', command=self.login).grid(row=3, column=2,pady=10,sticky='E')
        tk.Button(self.page, text='退出', command=self.page.quit).grid(row=3, column=1)

    def login(self,even=None):
        name = self.username.get()
        pwd = self.passwd.get()
        flag, message = db.check_login(name, pwd)
        if flag:
            self.page.destroy()
            MainPage(self.root)
        else:
            messagebox.showwarning(title='警告', message=message)
    
    def resource_path(self,relative):
        if hasattr(sys, "_MEIPASS"):
            absolute_path = os.path.join(sys._MEIPASS, relative)
        else:
            absolute_path = os.path.join(relative)
        return absolute_path

if __name__ == '__main__':
    root = tk.Tk(className=' USB借还管理工具 v1.0')
    LoginPage(root)
    root.mainloop()
