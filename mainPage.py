import tkinter as tk
from views import AboutFrame, ChangeFrame, InsertFrame, TurnBackFrame, UnReturnedFrame, ReBackFrame, LendoutFrame


class MainPage:
    def __init__(self, master: tk.Tk):
        self.root = master
        self.root.geometry('500x300+700+300')
        self.create_page()
        self.show_about()

    def create_page(self):
        self.about_frame = AboutFrame(self.root)
        self.change_frame = ChangeFrame(self.root)
        self.insert_frame = InsertFrame(self.root)
        self.turnback_frame = TurnBackFrame(self.root)
        self.unreturned_frame = UnReturnedFrame(self.root)
        self.reback_frame = ReBackFrame(self.root)
        self.lendout_frame = LendoutFrame(self.root)

        menubar = tk.Menu(self.root)
        menubar.add_command(label='借出', font=('黑体', 20), command=self.show_lendnout)
        menubar.add_command(label='归还', font=('黑体', 20), command=self.show_reback)
        menubar.add_command(label='未还', font=('黑体', 20), command=self.show_unreturned)
        menubar.add_command(label='已还', font=('黑体', 20), command=self.show_turnback)
        menubar.add_command(label='U盘库存', font=('黑体', 20), command=self.show_insert)
        menubar.add_command(label='修改密码', font=('黑体', 20), command=self.show_change)
        menubar.add_command(label='关于', font=('黑体', 20), command=self.show_about)
        menubar.add_command(label='退出', font=('黑体', 20), command=self.root.quit)
        self.root['menu'] = menubar

    def show_about(self):
        self.about_frame.pack()
        self.change_frame.pack_forget()
        self.insert_frame.pack_forget()
        self.turnback_frame.pack_forget()
        self.unreturned_frame.pack_forget()
        self.reback_frame.pack_forget()
        self.lendout_frame.pack_forget()

    def show_change(self):
        self.about_frame.pack_forget()
        self.change_frame.pack()
        self.insert_frame.pack_forget()
        self.turnback_frame.pack_forget()
        self.unreturned_frame.pack_forget()
        self.reback_frame.pack_forget()
        self.lendout_frame.pack_forget()

    def show_insert(self):
        self.about_frame.pack_forget()
        self.change_frame.pack_forget()
        self.insert_frame.pack()
        self.turnback_frame.pack_forget()
        self.unreturned_frame.pack_forget()
        self.reback_frame.pack_forget()
        self.lendout_frame.pack_forget()

    def show_turnback(self):
        self.about_frame.pack_forget()
        self.change_frame.pack_forget()
        self.insert_frame.pack_forget()
        self.turnback_frame.pack()
        self.unreturned_frame.pack_forget()
        self.reback_frame.pack_forget()
        self.lendout_frame.pack_forget()

    def show_unreturned(self):
        self.about_frame.pack_forget()
        self.change_frame.pack_forget()
        self.insert_frame.pack_forget()
        self.turnback_frame.pack_forget()
        self.unreturned_frame.pack()
        self.reback_frame.pack_forget()
        self.lendout_frame.pack_forget()

    def show_reback(self):
        self.about_frame.pack_forget()
        self.change_frame.pack_forget()
        self.insert_frame.pack_forget()
        self.turnback_frame.pack_forget()
        self.unreturned_frame.pack_forget()
        self.reback_frame.pack()
        self.lendout_frame.pack_forget()

    def show_lendnout(self):
        self.about_frame.pack_forget()
        self.change_frame.pack_forget()
        self.insert_frame.pack_forget()
        self.turnback_frame.pack_forget()
        self.unreturned_frame.pack_forget()
        self.reback_frame.pack_forget()
        self.lendout_frame.pack()


if __name__ == '__main__':
    root = tk.Tk(className=' USB借还管理工具 v1.0')
    MainPage(root)
    root.mainloop()
